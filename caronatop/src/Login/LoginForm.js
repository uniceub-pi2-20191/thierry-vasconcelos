import React, {Component} from 'react';
import {View, TextInput, StyleSheet, TouchableOpacity, Text, Alert} from 'react-native';

class LoginForm extends Component {

	constructor(props) {
	    super(props);
	    this.state = { email: '', pass: ''};
	}

	onPressButton() {
		Alert.alert(this.state.pass)
	}

	setEmail(email) {
		this.state.email = email
	}

	setPass(pass) {
		this.state.pass = pass
	}  

	render() {
		return (
			<View style={styles.container}>
				<TextInput style={styles.input}
						   onChangeText={(text) => this.setEmail(text)}
						   autoCapitalize='none'
						   autoCorrect={false}
						   keyboardType='email-address'
						   placeholder='Email'
						   placeholderColor='rgba(255,255,255,0.7)'
				/>
				<TextInput style={styles.input}
						   placeholder='Pass'
						   onChangeText={(text) => this.setPass(text)}
						   placeholderColor='rgba(255,255,255,0.7)'
						   secureTextEntry/>
				<TouchableOpacity style={styles.button} onPress={() => this.onPressButton()}>
					<Text style={styles.textButton}> Login </Text>
				</TouchableOpacity>
			</View>
		);
	}
}

const styles = StyleSheet.create({

	container: {
		padding: 20
	},

	input: {

		height: 60,
		width: 300,
		backgroundColor: 'rgba(255,255,255,0.2)',
		padding: 10,
		color: '#fff'

	},

	button: {
		height: 70,
		width: 300,
		padding: 15,
		backgroundColor: '#ff6505'
	},

	textButton: {
		color: '#fff',
		textAlign: 'center',
		fontWeight: '700',
		fontSize: 40

	}

});

export default LoginForm;
