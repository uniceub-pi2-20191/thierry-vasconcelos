import React, { Component } from 'react';
import { AppRegistry, View, Image, Text } from 'react-native';

export default class DisplayAnImage extends Component {
  render() {
    return (
       // Try setting `justifyContent` to `center`.
      // Try setting `flexDirection` to `row`.
      <View style={{
        justifyContent: 'center',
        alignItems: 'center'
      }}>
        <Image
          style={{width: 200, height: 150}}
          source={{uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/97/Cristo_Redentor_-_Rio_de_Janeiro%2C_Brasil-crop.jpg/290px-Cristo_Redentor_-_Rio_de_Janeiro%2C_Brasil-crop.jpg'}}
        />
        <Text> Cristo Redentor</Text>
        <Image
          style={{width: 200, height: 150}}
          source={{uri: 'https://cdn-istoe-ssl.akamaized.net/wp-content/uploads/sites/14/2018/08/2b2a4d28ab89d065f86169d5f8505eea312b9d66-590x720.jpg'}}
        />
        <Text> Estátua da Liberdade</Text>
      </View>
    );
  }
}